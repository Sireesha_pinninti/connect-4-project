import numpy as np

#Command line version of game

#Creating Game Board as a Matrix

ROW_COUNT = 6
COLUMN_COUNT = 7

def create_board():
    board = np.zeros((6,7))
    return board


board = create_board()
print(board)
game_over = False
turn = 0
piece = 0
draw = False
row = 0
column = 0


#Checking whether chosen column has an unoccupied row

def col_empty(board,column):
    return board[5][column] == 0

#Getting the appropriate  row in the chosen column to drop the piece

def get_row(board,column):
    for r in range(ROW_COUNT):
        if board[r][column] == 0:
            return r

#Dropping the piece at chosen place

def drop_piece(board,column,row,piece):
    board[row][column] = piece


#Flipping the matrix 
def print_board(board):
    print(np.flip(board,0))


def Finish_game(board,row,column,piece):
    return Horizantal_4(board,row,column,piece) or Vertical_4(board,row,column,piece) or diagonal_4(board,row,column,piece)


def Horizantal_4(board,row,column,piece):
    Coin = 1
    if column != 0:
        prev_col = column - 1
        while prev_col >= 0 and board[row][prev_col] == piece:
            Coin += 1
            if Coin == 4:
                return True
            prev_col -= 1
    if column != 6:
        next_col = column + 1
        while next_col <= 6 and board[row][next_col] == piece:
            Coin += 1
            if Coin == 4:
                return True
            next_col += 1
    return False


def Vertical_4(board,row,column,piece):
    Coin = 1
    if row != 0:
        prev_row = row - 1
        while prev_row >= 0 and board[prev_row][column] == piece:
            Coin += 1
            if Coin == 4:
                return True
            prev_row -= 1
    if row != 5:
        next_row = row + 1
        while next_row <= 5 and board[next_row][column] == piece:
            Coin += 1
            if Coin == 4:
                return True
            next_col += 1
    return False


def diagonal_4(board,row,column,piece):
    Coin = 1
    if row != 0 and column != 0:
        prev_row = row - 1
        prev_col = column - 1
        while prev_row >= 0 and prev_col >= 0 and board[prev_row][prev_col] == piece:
            Coin += 1
            if Coin == 4:
                return True
            prev_row -= 1
            prev_col -= 1
    if row != 5 & column != 6:
        next_row = row + 1
        next_col = column + 1
        while next_row <= 5 and  next_col <= 6 and board[next_row][next_col] == piece:
            Coin += 1
            if Coin == 4:
                return True
            next_row += 1
            next_col += 1
    if row != 5 and column != 0:
        next_row = row + 1
        prev_col = column - 1
        while next_row >= 0 and prev_col >= 0 and board[next_row][prev_col] == piece:
            Coin += 1
            if Coin == 4:
                return True
            next_row += 1
            prev_col -= 1
    if row != 5 & column != 6:
        prev_row = row - 1
        next_col = column + 1
        while prev_row <= 5 and  next_col <= 6 and board[prev_row][next_col] == piece:
            Coin += 1
            if Coin == 4:
                return True
            prev_row -= 1
            next_col += 1
 
    return False

def full_board(board):
    for col in range(COLUMN_COUNT):
        r = col_empty(board,col)
        if r == True:
            return False
    return True

#running the game loop until the game is over

while not game_over :
    if turn == 0:
        column  = int(input("(Player 1)Select a column from 0 to 6 : "))
        while column < 0 or column > 6:
            print("Select a column between (0 to 6) : ")
            column = int(input("(Player 1)Select a column from 0 to 6 : "))
        while not col_empty(board,column):
            column = int(input("Select another coloumn : "))
        row = get_row(board,column)
        piece = 1
        drop_piece(board,column,row,1)
    else :
        column = int(input("(Player 2)Select a column from 0 to 6 : "))
        while column < 0 or column > 6:
            print("Select a column between (0 to 6) : ")
            column = int(input("(Player 2)Select a column from 0 to 6 : "))
        while not col_empty(board,column):
            column = int(input("Select another column : "))
        row = get_row(board,column)
        piece = 2
        drop_piece(board,column,row,2) 
    turn += 1
    turn %= 2
    print_board(board)
    game_over = Finish_game(board,row,column,piece)
    if not game_over:
        if full_board(board):
            game_over = True
            draw =  True
print("Game over!")
if draw == True:
    print("it is a draw")
elif turn == 0:
    print("player 2 won")
else:
    print("player 1 won")


