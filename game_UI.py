import pygame
pygame.init()
font_name = pygame.font.get_default_font()
font = pygame.font.Font(font_name,40)

screen = pygame.display.set_mode((750,700))
screen.fill((0,0,255))

pos_x = [50,150,250,350,450,550,650]
pos_y = [50,150,250,350,450,550]
x = 50
for column in range(0,7):
    y = 50
    for row in range(0,6):
        pygame.draw.circle(screen,(255,255,255),(x,y),40)
        y += 100
    x += 100
pygame.display.update()

def col_empty(screen,x):
    if screen.get_at((x,50))[:3] == (255,255,255):
        return True
    return False

def get_row(screen,x):
    for y in range(550,49,-100):
        if screen.get_at((x,y))[:3] == (255,255,255):
            return y

def get_x_pos(screen,x,y):
    if y <= 590 and y >= 10:
        for pos in pos_x:
            if x <= pos + 40 and x >= pos - 40:
                return pos
    return 420
def finish_game(screen,y,x,colour):
    return horizantal_4(screen,y,x,colour) or vertical_4(screen,y,x,colour) or diagonal_4(screen,y,x,colour)


def horizantal_4(screen,y,x,colour):
    Coin = 1
    prev_col = x - 100
    while prev_col >= 50 and screen.get_at((prev_col,y))[:3] == colour:
        Coin += 1
        if Coin == 4:
            return True
        prev_col -= 100
    next_col =  x + 100
    while next_col <= 650 and screen.get_at((next_col,y))[:3] == colour:
        Coin += 1
        if Coin == 4:
            return True
        next_col += 100
    return False


def vertical_4(screen,y,x,colour):
    Coin = 1
    prev_row = y - 100
    while prev_row >= 50 and screen.get_at((x,prev_row))[:3] == colour:
        Coin += 1
        if Coin == 4:
            return True
        prev_row -= 100
    next_row = y + 100
    while next_row <= 550 and screen.get_at((x,next_row))[:3] == colour:
        Coin += 1
        if Coin == 4:
            return True
        next_row += 100
    return False


def diagonal_4(screen,y,x,colour):
    Coin = 1
    prev_row = y - 100
    prev_col = x - 100
    while prev_row >= 50 and prev_col >= 50 and screen.get_at((prev_row,prev_col))[:3] == colour:
        Coin += 1
        if Coin == 4:
            return True
        prev_row -= 100
        prev_col -= 100
    next_row = y + 100
    next_col = x + 100
    while next_row <= 550 and  next_col <= 650 and screen.get_at((next_row,next_col))[:3] == colour:
        Coin += 1
        if Coin == 4:
            return True
        next_row += 100
        next_col += 100
    next_row = y + 100
    prev_col = x - 100
    while next_row <= 550 and prev_col >= 50 and screen.get_at((next_row,prev_col))[:3] == colour:       
        Coin += 1
        if Coin == 4:
            return True
        next_row += 100
        prev_col -= 100
    prev_row = y - 100
    next_col = x + 100
    while prev_row >= 50 and  next_col <= 650 and screen.get_at((prev_row,next_col))[:3] == colour:
        Coin += 1
        if Coin == 4:
            return True
        prev_row -= 100
        next_col += 100
    return False

def full_board(screen):
    for x in pos_x:
        r = col_empty(screen,x)
        if r == True:
            return False
    return True

turn = 0
colour = (0,0,0)
running = True

while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                    x , y = event.pos[0], event.pos[1]
                    x = get_x_pos(screen,x,y)
                    if x in pos_x:
                        if col_empty(screen,x):
                            y = get_row(screen,x)
                            if turn % 2 == 0:
                                colour = (255,0,0)
                            else:
                                colour = (0,255,0)
                            pygame.draw.circle(screen,colour,(x,y),40)
                            pygame.display.update()
                            if finish_game(screen,y,x,colour):
                                if turn % 2 == 0:
                                    text = font.render('Game Over,red player won!',True,(0,255,255))
                                else:
                                    text = font.render('Game Over,green player won!',True,(0,255,255))
                                screen = pygame.display.set_mode((750,700))
                                screen.blit(text,(100,300))
                                pygame.display.update()
                            elif full_board(screen):
                                text = font.render('Game Over,It is a draw!',True,(0,255,255))
                                screen = pygame.display.set_mode((750,700))
                                screen.blit(text,(100,300))
                                pygame.display.update()
                            turn += 1
            
    
 



