#Basic Structure of game

import numpy

#Game Board as Matrix
board = numpy.zeros( ( 6 , 7 ) , dtype = int )

def drop_pieces(row,column,piece):
    board[row][column] = piece

def is_selected_empty(column):
    return board[0][column] == 0

def get_row(column):
    for r in range(6,0,-1):
        if board[r-1][column] == 0:
            return r-1
    

def play(piece):

    while True:
        column = int(input("Select a column between 0 and 6 : "))
        if column in range(0,7):
            if is_selected_empty(column):
                row = get_row( column )
                drop_pieces(row , column , piece)
                print(board)
                break
            else:
                print("Selected column is completely filled . Make another selection")
def Win_game():
    pass

def Start_game():
    is_done = False
    turn = 0
    while not is_done:
       #For Player1
        
        if turn % 2 == 0:
            print("Player1 it's your turn")
            play(1)

        #For player2
        else:
            print("player2 it's your turn")
            play(2)
        turn = turn + 1

Start_game()







