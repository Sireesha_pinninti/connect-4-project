import pygame


screen = pygame.display.set_mode((750,700))
screen.fill((0,0,255))

pos_x = [50,150,250,350,450,550,650]
pos_y = [50,150,250,350,450,550]
x = 50
for column in range(0,7):
    y = 50
    for row in range(0,6):
        pygame.draw.circle(screen,(255,255,255),(x,y),40)
        y += 100
    x += 100
pygame.display.update()

def col_empty(screen,x):
    if screen.get_at((x,50))[:3] == (255,255,255):
        return True
    return False

def get_row(screen,x):
    for y in range(550,49,-100):
        if screen.get_at((x,y))[:3] == (255,255,255):
            return y

def get_x_pos(screen,x,y):
    if y <= 590 and y >= 10:
        for pos in pos_x:
            if x <= pos + 40 and x >= pos - 40:
                return pos
    return 420
turn = 0
colour = (0,0,0)
running = True

while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                    x , y = event.pos[0], event.pos[1]
                    x = get_x_pos(screen,x,y)
                    if x in pos_x:
                        if col_empty(screen,x):
                            x = get_x_pos(screen,x,y)
                            y = get_row(screen,x)
                            if turn % 2 == 0:
                                colour = (255,0,0)
                            else:
                                colour = (0,255,0)
                            pygame.draw.circle(screen,colour,(x,y),40)
                            pygame.display.update()
                            turn += 1
            
    




