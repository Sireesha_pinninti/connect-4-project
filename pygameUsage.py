import pygame


screen = pygame.display.set_mode((750,700))
screen.fill((0,0,255))

def creating_board(screen):
    x = 50
    for column in range(0,7):
        y = 50
        for row in range(0,6):
            pygame.draw.circle(screen,(255,255,255),(x,y),40)
            y += 100
        x += 100

board = creating_board(screen)


pygame.display.update()

def get_row(screen,x):
    for y in range(50,650):
        if screen.get_at((x,y))[:3] == (255,255,255):
            return y
turn = 0

running = True

while running:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    if turn % 2 == 0:
        pygame.draw.circle(screen,(255,255,255),(250,650),40)
        pygame.draw.circle(screen,(255,0,0),(250,650),40)
        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                x = event.pos[1]
                y = get_row(screen,x)
                pygame.draw.circle(screen,(255,0,0),(x,y),40)
                pygame.display.update()
            
    




