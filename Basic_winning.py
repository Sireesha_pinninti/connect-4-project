import numpy as np

#Command line version of game

#Creating Game Board as a Matrix

ROW_COUNT = 6
COLUMN_COUNT = 7

def create_board():
    board = np.zeros((6,7),dtype = int)
    return board


#Checking whether chosen column has an unoccupied row

def col_empty(board,column):
    return board[0][column] == 0

#Getting the appropriate  row in the chosen column to drop the piece

def get_row(board,column):
    for r in range(ROW_COUNT , 0 , -1):
        if board[r-1][column] == 0:
            return r-1

#Dropping the piece at chosen place

def drop_piece(board,column,row,piece):
    board[row][column] = piece


def horizontal_4(board,row,column,piece):
    coin = 1
    prev_col = column - 1
    while prev_col >= 0 and board[row][prev_col] == piece:
        coin += 1
        if coin == 4: 
            return True
        prev_col -= 1
    next_col = column + 1
    while next_col <= 6 and board[row][next_col] == piece:
        if coin == 4:
            return True
        count += 1
        next_col += 1
    return False


def vertical_4(board,row,column,piece):
    coin = 1
    prev_row = row - 1
    while prev_row >= 0 and board[prev_row][column] == piece:
        coin += 1
        if coin == 4:
            return True
        prev_row -= 1
    next_row = row + 1
    while next_row <= 5 and board[next_row][column] == piece:
        coin += 1
        if coin == 4:
            return True
        next_row += 1
    return False


def diagonal_4(board,row,column,piece):
    coin = 1
    prev_row = row - 1
    prev_col = column - 1
    while prev_row >= 0 and prev_col >= 0 and board[prev_row][prev_col] == piece:
        coin += 1
        if coin == 4:
            return True
        prev_row -= 1
        prev_col -= 1
    next_row = row + 1
    next_col = column + 1
    while next_row <= 5 and  next_col <= 6 and board[next_row][next_col] == piece:
        coin += 1
        if coin == 4:
            return True
        next_row += 1
        next_col += 1
    next_row = row + 1
    prev_col = column - 1
    while next_row <= 5 and prev_col >= 0 and board[next_row][prev_col] == piece:
        coin += 1
        if coin == 4:
            return True
        next_row += 1
        prev_col -= 1
    prev_row = row - 1
    next_col = column + 1
    while prev_row >= 0 and  next_col <= 5 and board[prev_row][next_col] == piece:
        coin += 1
        if coin == 4:
            return True
        prev_row -= 1
        next_col += 1

    return False
def finish_game(board,row,column,piece):
    return horizontal_4(board,row,column,piece) or vertical_4(board,row,column,piece) or diagonal_4(board,row,column,piece)

def full_board(board):
    for col in range(COLUMN_COUNT):
        r = col_empty(board,col)
        if r == True:
            return False
    return True

def to_select_column():
    column = int(input("Select a column between 0 and 6 : "))
    while column not in range(7):
        print("Selected row is not valid.Make another choice :")
        column = int(input("Select a column between 0 and 6 : "))
    while not col_empty(board,column):
        print("Selected column is filled.Make another choice")
        column = int(input())
    return column

#running the game loop until the game is over
turn = 0
piece = 0
draw = False
game_over = False
board = create_board()
print(board)
while not game_over :
    if turn % 2 ==0:
        print("Player1 its your turn")
        column = to_select_column()
        row = get_row(board,column)
        drop_piece(board,column,row,1)
        piece = 1
    else :
        print("player2 its your turn")
        column = to_select_column()
        row = get_row(board,column)
        drop_piece(board,column,row,2)
        piece = 2
    turn += 1
    print(board)
    game_over = finish_game(board,row,column,piece)
    if not game_over:
        if full_board(board):
            draw =  True
            break
print(board)
print("Game over!")
if draw == True:
    print("it is a draw")
elif turn % 2 == 0:
    print("player 2 won")
else:
    print("player 1 won")


