import pygame


screen = pygame.display.set_mode((700,600))
screen.fill((0,0,255))

pos_x = [50,150,250,350,450,550,650]
pos_y = [50,150,250,350,450,550]
x = 50
for column in range(0,7):
    y = 50
    for row in range(0,6):
        pygame.draw.circle(screen,(255,255,255),(x,y),40)
        y += 100
    x += 100
pygame.display.update()

def col_empty(screen,x):
    if screen.get_at((x,50)) == (255,255,255):
        return True
    return False

def get_row(screen,x):
    for y in range(550,49,-100):
        if screen.get_at((x,y)) == (255,255,255):
            return y

def get_x_pos(screen,x,y):
    if y <= 590 and y >= 10:
        for pos in pos_x:
            if x <= pos + 40 and x >= pos - 40:
                return pos
    return 420
def horizontal_4(screen,x_coordinate,y_coordinate,colour):
    coin = 1
    prev_x_coordinate = x_coordinate - 100
    while prev_x_coordinate >= 50 and screen.get_at((prev_x_coordinate,y_coordinate)) == colour:
        coin += 1
        if coin == 4:
            return True
        prev_x_coordinate -= 100
    next_x_coordinate = x_coordinate + 100
    while next_x_coordinate <= 650 and screen.get_at((next_x_coordinate,y_coordinate)) == colour:
        coin += 1
        if coin == 4:
            return True
        next_x_coordinate += 100
    return False

def vertical_4(screen,x_coordinate,y_coordinate,colour):
    coin = 1
    prev_y_coordinate = y_coordinate - 100
    while prev_y_coordinate >= 50 and screen.get_at((x_coordinate,prev_y_coordinate)) == colour:
        coin += 1
        if coin == 4:
            return True
        prev_y_coordinate -= 100
    next_y_coordinate = y_coordinate + 100
    while next_y_coordinate <= 550 and screen.get_at((x_coordinate,next_y_coordinate)) == colour:
        coin += 1
        if coin == 4:
            return True
        next_y_coordinate += 100
    return False
def winning_move(screen,x_coordinate,y_coordinate,colour):
    return horizontal_4(screen,x_coordinate,y_coordinate,colour) or vertical_4(screen,x_coordinate,y_coordinate,colour)

turn = 0
colour = (0,0,0)
running = True
game_over = False

while running and not game_over:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                    x , y = event.pos
                    x = get_x_pos(screen,x,y)
                    if x in pos_x:
                        if col_empty(screen,x):
                            y = get_row(screen,x)
                            if turn % 2 == 0:
                                colour = (255,0,0)
                      
                            else:
                                colour = (255,255,0)
                            pygame.draw.circle(screen,colour,(x,y),40)
                            pygame.display.update()
                            if winning_move(screen,x,y,colour):
                                game_over = True
                                break
                            turn += 1

            
    




